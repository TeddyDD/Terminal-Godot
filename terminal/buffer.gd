extends Reference

var size # size of buffer: Vector2

var chars    # array of chars
var fgcolors # foreground (text) colors
var bgcolors # background colors

# params: columns, rows, default foreground and backgroud colors)
func _init(c,r, fg, bg):
	size = Vector2(c, r)
	
	# initialize buffers
	chars = []
	fgcolors = []
	bgcolors = []
	set_default(" ",fg,bg)

# returns index for given column and row
func index(point):
	var column = point.x
	var row = point.y
	return ((row-1) * size.width) + (column - 1)
	
func set_default(c, fg, bg):
	# resize buffers
	var b = size.width * size.height
	
	chars.resize(b)
	fgcolors.resize(b)
	bgcolors.resize(b)
	
	# set default variables
	for item in range( b ):
		chars[item] = c
		fgcolors[item] = fg
		bgcolors[item] = bg
