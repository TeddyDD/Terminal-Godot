# TERMINAL
# This class deals only with drawing terminal
# It contains a buffer of current terminal content
# The interaction and behavior must be defined in external controller, using api this class.

extends Control


# Exported variables

# Minimal ammount of columns and rows
# get_minimum_size() is calculated based on these values
export(int) var min_columns = 1
export(int) var min_rows = 1

export(Color) var foregound_default  # default text color
export(Color) var background_default # default background color
export(Font) var font                # monospaced font

# offset of characters in cells
export(int) var font_x_offset = 0    # move the character in a cell in the x-axis
export(int) var font_y_offset = 0    # move the character in a cell in the y-axis

# change the size of cells
# The size of the cells is calculated based on size of "W" character
# These properties allow you to change the margin of characters
export(int) var resize_cell_x = 0
export(int) var resize_cell_y = 0


# Global variables

var columns = 0
var rows = 0

var cell = Vector2( 0, 0) # size of one cell

# buffer with the contents of the screen
# better do not acess this directly from external controller
# TODO aloow custom buffers
var Buffer = preload("buffer.gd")
var buff 

# Public API

func _draw():
	#draw background
	draw_rect( Rect2(0,0, cell.width * columns, cell.height * rows), background_default)
	#draw letters and boxes
	for r in range(rows):
		for c in range(columns):
			# index
			var i = buff.index(Vector2(c+1,r+1))
			
			# draw bg 
			draw_rect( Rect2(c * cell.width, r * cell.height, cell.width, cell.height) , buff.bgcolors[i] )
			
			# draw text
			var font_pos = Vector2((c * cell.width) + font_x_offset,((r + 1) * cell.height) + font_y_offset)
			draw_char( font, font_pos, buff.chars[i], "W", buff.fgcolors[i])

# Set colors of given cell
func write_color(c, r, fg, bg):
	buff.fgcolors[buff.index(Vector2(c, r))] = fg
	buff.bgcolors[buff.index(Vector2(c, r))] = bg

# Set character in given cell
func write_char(c, r, char):
	buff.chars[buff.index(Vector2(c,r))] = char

func write_string(c, r, string, fg, bg):
	var cursor = Vector2(c,r)
	for letter in range(string.length()):
		var char = string[letter]
		var i = buff.index(Vector2(cursor.x, cursor.y))
		buff.chars[i] = char
		buff.fgcolors[i] = fg
		if (bg != null):
			buff.bgcolors[i] = bg
		
		# line wrap
		if (cursor.x != columns):
			cursor.x += 1
		elif (cursor.x == columns):
			cursor.x = 1
			cursor.y += 1
			if (cursor.y > rows):
				return


# Draw rectangle with given color
func write_rectangle(rect, bg):
	var cursor = rect.pos
	for r in range(rect.pos.y, rect.pos.y + rect.size.height):
		if (r > rows-1):
			continue
			
		for c in range(rect.pos.x, rect.pos.x + rect.size.width):
			if (c > columns-1):
				continue
			
			buff.bgcolors[buff.index(Vector2(c+1,r+1))] = bg


# clears the buffer with defaut varialbes
func write_clean():
	buff.set_default(" ", foregound_default, background_default)

# internal functions

func _ready():
	calculate_size()
	
	# create buffer
	# TODO resizing - buffer swaping
	buff = Buffer.new(columns, rows, foregound_default, background_default)

func calculate_size():
	var width = get_size().width
	var height = get_size().height
	
	cell.width = int(font.get_string_size("W").width) + resize_cell_x
	cell.height = int(font.get_height()) + resize_cell_y
	
	# make sure we have more columns than min_columns
	while (columns <= min_columns):
		columns = ( width - (int(width) % int(cell.width)) ) / cell.width
		if (columns >= min_columns):
			break
		else:
			width += (min_columns - columns) * cell.width
	# TODO set minimal size and resizing
	
	# make sure we have more rows than min_rows
	while (rows <= min_rows):
		rows = ( height - (int(height) % int(cell.height)) ) / cell.height
		if (rows >= min_rows):
			break
		else:
			height += (min_rows - rows) * cell.height




