
extends Panel

func _ready():
	#get terminal
	var term = get_node("Terminal")
	
	var color = 0

#	Uncomment for debug
#	prints("Size",term.columns, term.rows)
	
	# Range is quiet tricki when iteratin over columns and rows
	for r in range(1, term.rows + 1):
		for c in range(1, term.columns + 1):
			term.write_color(c, r, Color(color,0,color), Color(color, 0, 0))
			term.write_char(c, r, '#')
			color += 1.0/ (term.columns * term.rows)
	
	var r = Rect2(Vector2(10,10), Vector2(5,3))
	term.write_rectangle(r, Color(0.475699, 0.228866, 0.560133))
	term.update()


